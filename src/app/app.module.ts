import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppComponent } from './app.component';
import { RebelConNavModule } from './rebel-con-nav/rebel-con-nav.module';
import { RouterModule, ROUTES, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule, ActionReducerMap, State } from '@ngrx/store';
import { TestDogsComponent } from "./TestDogs.component";
import { TestCatsComponent } from "./TestCats.component";
import { TestCowsComponent } from "./TestCows.component.";
import { TestHensComponent } from "./TestHens.component";
import { reducers, CustomSerializer } from './store/reducers/reducers';
import {
  StoreRouterConnectingModule,
  RouterStateSerializer
} from '@ngrx/router-store';

// routes
export const APP_ROUTES: Routes = [
  {
    path: '',
    redirectTo: '/dogs',
    pathMatch: 'full'
  },
  {
    path: 'dogs',
    component: TestDogsComponent
  },
  {
    path: 'cats',
    component: TestCatsComponent
  },
  {
    path: 'cows',
    component: TestCowsComponent
  },
  {
    path: 'hens',
    component: TestHensComponent
  }
];

@NgModule({
  declarations: [AppComponent, TestCatsComponent, TestCowsComponent, TestDogsComponent, TestHensComponent],
  imports: [
    BrowserModule,
    RebelConNavModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(APP_ROUTES),
    StoreModule.forRoot(reducers),
    StoreRouterConnectingModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
