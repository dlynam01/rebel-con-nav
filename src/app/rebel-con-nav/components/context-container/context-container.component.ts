import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
  selector: `app-context-container`,
  template: `
    <ng-content></ng-content>
  `
})
export class ContextContainerComponent {
  constructor() {}
}
