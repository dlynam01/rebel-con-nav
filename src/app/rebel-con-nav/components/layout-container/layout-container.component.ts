import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
  selector: 'app-layout-container',
  styles: [
    `
    :host {
      padding: 50px 30px 0 230px;
      transition: all .4s cubic-bezier(.770,0,.175,1);
    }

    .is-collapsed:host {
      padding-left: 90px;
    }
  `
  ],
  template: `
    <app-top-nav></app-top-nav>
    <app-side-nav></app-side-nav>
    <app-context-container class="container">
      <ng-content></ng-content>
    </app-context-container>
  `
})
export class LayoutContainerComponent {
  @HostBinding('attr.class') class = 'd-flex';

  constructor() {}
}
