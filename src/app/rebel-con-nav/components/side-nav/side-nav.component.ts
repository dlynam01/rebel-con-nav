import { Component, OnInit, HostBinding } from '@angular/core';
import { NavState } from '../../store/reducers/reducers';
import { SIDE_NAV_ITEMS_VIEW } from '../../store/views/views';
import { Store } from '@ngrx/store';
import { SideNavItem } from '../../models/SideNavItem';
import { NavService } from '../../services/nav.service';

@Component({
    selector: 'app-side-nav',
    styles: [`
    :host {
      position: fixed;
      top: 50px;
      bottom: 0;
      left: 0;
      z-index: 1030;
      width: 200px;
      font-size: .865rem;
      color: white;
      user-select: none;
      transition: all .4s cubic-bezier(.770,0,.175,1);
    }

    .item {
      position: relative;
      text-decoration: none;
      color: currentColor;
      perspective: 1000px;
      transition: all .3s;
    }

    .item--toggle {
      overflow: hidden;
    }

    .item:hover {
      background: var(--dark-blue-80);
    }

    .item__icon {
      position: relative;
      z-index: 2;
      flex-shrink: 0;
      width: 60px;
      font-size: 1rem;
      transition: all .3s;
    }

    .item__label {
      position: relative;
      padding: 15px 20px;
      margin-left: -20px;
      background: transparent;
      transform-origin: left center;
      transition: all .3s;
    }

    /* Item Arrows */

    .item::before {
      content: "";
      position: absolute;
      top: 50%;
      right: 0;
      opacity: 0;
      color: transparent;
      border-right: 5px solid var(--gray-10);
      border-top: 5px solid transparent;
      border-bottom: 5px solid transparent;
      transform: translate(100%, -50%);
      transition: all .4s cubic-bezier(.770,0,.175,1);
    }

    /* Collapsed */

    .is-collapsed:host {
      width: 60px;
    }

    .is-collapsed:host .item--toggle .item__icon {
      transform: rotate(180deg);
    }

    .is-collapsed:host .item__label {
      opacity: 0;
      visibility: hidden;
      transform: translateX(-50%) rotateY(0);
    }

    .is-collapsed:host .item:hover:not(.item--toggle) .item__label {
      opacity: 1;
      visibility: visible;
      background: var(--dark-blue-80);
      transform: translateX(0);
    }

    .is-collapsed:host .item--toggle:hover .item__label {
      opacity: 0;
      visibility: hidden;
    }

    /* Active */

    .item--active,
    .item--active:hover,
    :host .item--active:hover .item__label {
      color: var(--white);
      background: var(--dark-blue-70) !important;
    }

    .item--active::before {
      opacity: 1;
      transform: translate(0, -50%);
    }
  `],
    template: `
    <div class="d-flex flex-column align-self-stretch h-100">
      <a href class="item d-flex align-items-center"
         [ngClass]="{ 'item--active': item.isActive, 'mt-auto': item.isBottom }"
         [routerLink]='item.link'
         *ngFor='let item of sideNavItems'>
        <i class="item__icon fas fa-fw {{item.icon}}"></i>
        <span class="item__label">
          {{item.name}}
         </span>
      </a>
    </div>
    <div href class='item item--toggle d-flex align-items-center text-dark-blue-40' (click)='toggleSideBar()'>
      <i class="item__icon fas fa-fw fa-angle-double-left"></i>
      <span class="item__label">{{ !isCollapsed ? 'Collapse' : 'Expand' }}</span>
    </div>
  `
})
export class SideNavComponent implements OnInit {
    @HostBinding('class.is-collapsed') isCollapsed;
    @HostBinding('attr.class') class = 'd-flex flex-column justify-content-between bg-dark-blue-100';

    sideNavItems: SideNavItem[] = [];

    constructor(private store: Store<NavState>, private navService: NavService) {
        this.navService.isCollapsed$().subscribe(state => this.isCollapsed = state);
    }

    ngOnInit(): void {
        this.store
            .select(SIDE_NAV_ITEMS_VIEW)
            .subscribe(sideNavItems => (this.sideNavItems = sideNavItems));
    }

    toggleSideBar() {
        this.navService.toggleExpand();
    }
}
