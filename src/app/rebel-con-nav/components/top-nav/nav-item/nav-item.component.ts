import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { NavLink } from '../../../models/NavLink';
import { Store } from '@ngrx/store';
import * as fromViews from '../../../store/views/views';
import { ClearSideNavItems } from '../../../store/actions/nav.actions';

@Component({
  selector: 'app-nav-item',
  template: `
     <a class="item" *ngIf="selectedLink$ |async as selectedLink"
      [class.item--active]="selectedLink.startsWith(nav.link)"
      [routerLink]="nav.link"
      (click)="clearSideNavItems(!selectedLink.startsWith(nav.link))">
      {{nav.name}}
    </a>
  `,
  styles: [`
  .item {
    padding: 13px 20px;
    text-decoration: none;
    color: currentColor;
    transition: all .2s;
  }

  .item:hover,
  .item:focus {
    background: var(--dark-blue-80);
  }

  .item--active,
  .item--active:hover,
  .item--active:focus {
    color: var(--dark-blue-100);
    background: var(--turquoise-100);
  }
  `]
})
export class NavItemComponent implements OnInit {
  selectedLink$: Observable<string>;
  @Input() nav: NavLink;
  @HostBinding('attr.class') class = 'd-inline-flex align-items-center';

  constructor(private store: Store<any>) {}

  ngOnInit(): void {
    this.selectedLink$ = this.store.select(fromViews.SELECTED_CONTEXT_VIEW);
  }

  clearSideNavItems(shouldClear) {
    if (shouldClear) {
      this.store.dispatch(new ClearSideNavItems());
    }
  }

}
