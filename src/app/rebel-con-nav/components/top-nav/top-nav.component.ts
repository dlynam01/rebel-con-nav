import {
  Component,
  OnInit,
  style,
  state,
  animate,
  transition,
  trigger,
  HostBinding
} from '@angular/core';
import { NavLink } from '../../models/NavLink';
import { NavService } from "../../services/nav.service";

@Component({
  selector: 'app-top-nav',
  template: `<nav class="nav">
                <a [ngClass]="{'is-collapsed': isCollapsed}" class="logo">
                  <div *ngIf="isCollapsed">
                    <svg xmlns="http://www.w3.org/2000/svg" width="60" height="50">
                      <path
                        fill="#3DE3C9"
                        fill-rule="evenodd"
                        d="M29.201 14H22v21.975l4.936-6.353h2.568c5.996 0 9.297-3.562 9.297-7.971 0-4.673-3.513-7.65-9.6-7.65"/>
                    </svg>
                  </div>
                  <div *ngIf="!isCollapsed">
                    <svg xmlns="http://www.w3.org/2000/svg" width="200" height="50">
                      <path
                        fill="#3DE3C9"
                        fill-rule="evenodd"
                        d="M29.201 12c6.087 0 9.6 2.978 9.6 7.65 0 4.41-3.3 7.972-9.297 7.972h-2.568L22 33.975V12h7.201zm.546 11.388c2.392 0 4.058-1.285 4.058-3.562 0-2.278-1.666-3.475-4.089-3.475h-2.78v7.037h2.81zM48.584 33.42c-4.936 0-8.812-3.475-8.812-8.497 0-5.052 3.876-8.497 8.812-8.497 4.997 0 8.813 3.445 8.813 8.497 0 4.993-3.816 8.497-8.813 8.497zm0-12.79c-1.998 0-3.906 1.548-3.906 4.293 0 2.744 1.908 4.292 3.906 4.292 2 0 3.937-1.577 3.937-4.292 0-2.745-1.938-4.293-3.937-4.293zm15.961 11.176v6.773H59.7V16.834h3.03l1.06 1.741c1.234-1.274 3.326-2.15 5.372-2.15 3.906 0 8.071 3.33 8.071 8.498 0 5.197-4.044 8.497-7.98 8.497-1.621 0-3.465-.615-4.708-1.614zm3.846-11.176c-2.271 0-3.906 1.782-3.906 4.293s1.635 4.292 3.876 4.292c2.301 0 3.967-1.84 3.967-4.321 0-2.482-1.666-4.264-3.937-4.264zm16.021 11.176v6.773h-4.845V16.834h3.031l1.06 1.74c1.234-1.274 3.325-2.148 5.371-2.148 3.907 0 8.072 3.329 8.072 8.497 0 5.197-4.044 8.497-7.98 8.497-1.622 0-3.466-.616-4.709-1.614zM88.26 20.63c-2.271 0-3.907 1.782-3.907 4.293s1.636 4.292 3.876 4.292c2.302 0 3.967-1.84 3.967-4.321 0-2.482-1.665-4.264-3.936-4.264zm18.628 12.79c-4.754 0-7.601-2.891-7.601-6.979v-9.607h4.846v9.607c0 1.81.999 2.774 2.755 2.774 1.787 0 2.847-.964 2.847-2.803v-9.578h4.845v9.607c0 4.088-2.907 6.979-7.692 6.979zm10.34-.41V12h4.845v21.01h-4.845zm15.96.41c-4.935 0-8.811-3.475-8.811-8.497 0-5.052 3.876-8.497 8.812-8.497 4.996 0 8.812 3.445 8.812 8.497 0 4.993-3.816 8.497-8.812 8.497zm0-12.79c-1.998 0-3.906 1.548-3.906 4.293 0 2.744 1.908 4.292 3.907 4.292s3.937-1.577 3.937-4.292c0-2.745-1.938-4.293-3.937-4.293z"/>
                    </svg>
                  </div>
                 </a>
                <app-nav-item *ngFor="let link of links" [nav]="link"></app-nav-item>
              </nav>
`,
  styles: [``]
})
export class TopNavComponent implements OnInit {
  @HostBinding('attr.class') class = 'fixed-top d-flex justify-content-between bg-dark-blue-100 text-white';
  public isCollapsed: boolean;

  constructor(poppuloNavService: NavService) {
    poppuloNavService.isCollapsed$().subscribe(state => this.isCollapsed = state);
  }

  links: Array<NavLink> = [
    { name: 'Dogs', link: '/dogs' },
    { name: 'Cats', link: '/cats' },
    { name: 'Hens', link: '/hens' },
    { name: 'Cows', link: '/cows' }
  ];

  ngOnInit() {}
}
