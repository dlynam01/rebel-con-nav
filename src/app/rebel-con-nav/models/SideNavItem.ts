export class SideNavItem {
    constructor(public name: string, public isActive: boolean, public icon: string, public link: string, public isBottom: boolean) {}
}
