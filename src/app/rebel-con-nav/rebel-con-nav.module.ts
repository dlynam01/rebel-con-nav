import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopNavComponent } from './components/top-nav/top-nav.component';
import { NavItemComponent } from './components/top-nav/nav-item/nav-item.component';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { ContextContainerComponent } from './components/context-container/context-container.component';
import { LayoutContainerComponent } from './components/layout-container/layout-container.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { reducer } from './store/reducers/reducers';
import { NavService } from "./services/nav.service";

@NgModule({
  imports: [CommonModule, StoreModule.forFeature('nav', reducer), RouterModule],
  declarations: [
    TopNavComponent,
    NavItemComponent,
    ContextContainerComponent,
    LayoutContainerComponent,
    SideNavComponent
  ],
  providers: [NavService],
  exports: [LayoutContainerComponent]
})
export class RebelConNavModule { }
