import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class NavService {
  private isCollapsed = false;
  private isCollapsedSub = new Subject<boolean>();
  public isCollapsedSub$ = this.isCollapsedSub.asObservable();

  public toggleExpand() {
    this.isCollapsed = !this.isCollapsed;
    this.isCollapsedSub.next(this.isCollapsed);
  }

  public isCollapsed$() {
    return this.isCollapsedSub$;
  }
}
