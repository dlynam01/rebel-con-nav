import { Action } from '@ngrx/store';
import { NavLink } from '../../models/NavLink';
export const CLEAR_SIDE_NAV_ITEMS = '[Side-Nav] Clear Side-Nav Items';

export class ClearSideNavItems implements Action {
  readonly type = CLEAR_SIDE_NAV_ITEMS;
  constructor() {}
}

export type NAV_ACTIONS = ClearSideNavItems;
