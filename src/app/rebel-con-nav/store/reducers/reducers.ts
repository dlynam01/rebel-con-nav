import * as fromActions from '../actions/nav.actions';
import { createFeatureSelector, MemoizedSelector } from '@ngrx/store';
import { NavLink } from '../../models/NavLink';

export interface NavState {
}

const initialState: NavState = {
};

export function reducer(
  state: NavState = initialState,
  action: fromActions.NAV_ACTIONS
): NavState {
  return state;
}

export const NAV_VIEW = createFeatureSelector('nav');
