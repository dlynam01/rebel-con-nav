export const SET_SIDE_NAV_ITEMS = '[Side-Nav] Side-Nav Items';
export const CLEAR_SIDE_NAV_ITEMS = '[Side-Nav] Clear Side-Nav Items';

export class SideNavItem {
    constructor(public name: string, public isActive: boolean, public icon: string, public link: string, ) { }
}
export interface SideNavState {
    sideNavItems: SideNavItem[];
}

export function sideNavReducer(
    state: SideNavState,
    action: any
): SideNavState {
    switch (action.type) {
        case SET_SIDE_NAV_ITEMS: {
            const sideNavItems = action.sideNavItems;
            return {
                ...state,
                sideNavItems
            };
        }
        case CLEAR_SIDE_NAV_ITEMS: {
            return {
                ...state,
                sideNavItems: []
            };
        }
        default:
            return state;
    }
}
