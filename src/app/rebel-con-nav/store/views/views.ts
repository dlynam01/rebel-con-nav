import { NAV_VIEW, NavState } from '../reducers/reducers';
import {
  createFeatureSelector,
  createSelector,
  MemoizedSelector
} from '@ngrx/store';
import { SideNavItem } from '../../models/SideNavItem';

const ROUTER_VIEW = createFeatureSelector('routerReducer');
const SIDE_NAV_VIEW = createFeatureSelector('sideNavState');

interface ROUTER_NAVIGATION_STATE {
  state: { url: string };
}

interface SideNavItemsState {
  sideNavItems: SideNavItem[];
}

export const SIDE_NAV_ITEMS_VIEW = createSelector(
  SIDE_NAV_VIEW,
  (state: SideNavItemsState) => (state ? state.sideNavItems : [])
);

export const SELECTED_CONTEXT_VIEW = createSelector(
  ROUTER_VIEW,
  (state: ROUTER_NAVIGATION_STATE) => (state ? state.state.url : '')
);
